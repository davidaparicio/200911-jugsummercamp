# La sécurité dès la conception du projet, Jug Summer Camp 2020

🚀 How to run it ?

```bash
hugo server
open localhost:1313
# To print into PDF
# -> localhost:1313/?print-pdf&showNotes=false&pdfSeparateFragments=false
```

🕸️ How install dependencies ?

```bash
brew install hugo
git submodule update --recursive --remote --init
# or
# git submodule init && \
# git submodule add git@github.com:dzello/reveal-hugo.git themes/reveal-hugo
```

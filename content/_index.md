+++
title = "200911 Jug Summer Camp 2020, La sécurité dès la conception du projet"
outputs = ["Reveal"]
# layout = "bundle"
[reveal_hugo]
theme = "white"
slide_number = true
# history = true
# transition = "slide"
# transition_speed = "fast"
[reveal_hugo.templates.jsc]

class = "jsc"
# background = "#badfe9"
background-image = "img/jsc2020_background.png"
# https://developer.mozilla.org/en-US/docs/Web/CSS/background-position
# https://developer.mozilla.org/en-US/docs/Web/CSS/background-size
background-size = "contain"

# https://github.com/dzello/reveal-hugo/issues/19
# https://themes.gohugo.io/theme/reveal-hugo/#/14
# https://revealjs.com/themes/
# black	Black background, white text, blue links (default)
# white	White background, black text, blue links
# league	Gray background, white text, blue links
# beige	Beige background, dark text, brown links
# sky	Blue background, thin dark text, blue links
# night	Black background, thick white text, orange links
# serif	Cappuccino background, gray text, brown links
# simple	White background, black text, blue links
# solarized	Cream-colored background, dark green text, blue links
# blood	Dark background, thick white text, red links
# moon	Dark blue background, thick grey text, blue links
+++

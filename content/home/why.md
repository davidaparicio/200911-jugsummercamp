+++
weight = 5
+++
{{< slide template="jsc" >}}
{{% section %}}
## Why?

![OWASP](img/owasp.png)
[OWASP Top 10](https://tinyurl.com/api-owasp)

---
{{< slide template="jsc" >}}
#### Gendarmerie Nationale

> L’entrée en vigueur du [RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es) modifie la posture des acteurs (des traitements) qui doivent tenir compte des impératifs de sécurité dès la conception d’un produit ainsi que son cycle de vie. Le label [« by design »](https://www.gendarmerie.interieur.gouv.fr/crgn/content/download/1033/16080/) devient un label de qualité qui constituera un atout commercial.

---
{{< slide template="jsc" >}}
## 2022

* 90% des développements logiciels se déclaront DevSecOps (+40% 2019)
* 25% des développements IT selon DevOps (+10% 2019)

[Gartner/Techwire](https://www.techwire.net/sponsored/integrating-security-into-the-devsecops-toolchain.html)

---
{{< slide template="jsc" >}}
#### Et pour éviter cela
![Windows 3.1](img/why_sec.png)

---
{{< slide template="jsc" >}}
#### Ou ceci
![Crypto](img/cyber.jpg)

---
{{< slide template="jsc" >}}
#### Risque humain ?

![Gigafactory Tesla Malware](img/tesla_malware.png)
[Arstechnica](https://arstechnica.com/information-technology/2020/08/russian-tourist-offered-employee-1-million-to-cripple-tesla-with-malware/)

---
{{< slide template="jsc" >}}
#### Snyk TL;DR
![TL;DR](img/snyk_takeaways.png)

{{% /section %}}

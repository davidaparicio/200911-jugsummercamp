+++
weight = 6
+++
{{< slide template="jsc" >}}
{{% section %}}
## Pour aller plus loin

* [Sophia Security Camp 2019](http://www.telecom-valley.fr/sophia-security-camp-2019/)
* [ANSSI Sécurité Agile](https://www.ssi.gouv.fr/administration/guide/agilite-et-securite-numeriques-methode-et-outils-a-lusage-des-equipes-projet/) (Atelier d'analyse de risque)
![Guide de l'ANSSI, Agilité & Sécurité numériques](img/anssi_agile.jpg)

---
{{< slide template="jsc" >}}
## Analogie
> « Nul n'est censé ignorer la loi »

---
{{< slide template="jsc" >}}
## Ma devise
> « Nul développeur n'est censé ignorer la sécurité »

---
{{< slide template="jsc" >}}
## David Aparicio

15/ DD INSA de Lyon / UNICAMP (Brésil)
Facebook Open Academy / MIT AppInventor

17/ Dev(Sec)Ops @ AMADEUS (Nice, 2 ans)

19/ DataOps @ OVHCloud (Lyon, 2 ans)

---
{{< slide template="jsc" >}}
## OVH
![OVH Datacenters](img/ovh_map.jpg)

<!-- Curieux
* Transactional Memories
* Battery Profiling
* MIT AppInventor
* SMPC / BC -->
---
{{< slide template="jsc" >}}
## Merci
![oscar](img/thanks.gif)

---
{{< slide template="jsc" >}}
### La question est vite répondue
![chien](img/security_container_small.gif)

{{% /section %}}
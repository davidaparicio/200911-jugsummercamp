+++
weight = 3
+++
{{< slide template="jsc" >}}
{{% section %}}

### Sécurité dès la conception

95/ SdD-"Privacy By Design"

97/ Loi allemande

10-12/ Congrès EU

2016/679/ GDPR

---
{{< slide template="jsc" >}}
### DevSecOps

![DevSecOps](/img/devsecops.png)
[dodcio.defense.gov](https://dodcio.defense.gov/Portals/0/Documents/DoD%20Enterprise%20DevSecOps%20Reference%20Design%20v1.0_Public%20Release.pdf?ver=2019-09-26-115824-583)

---
{{< slide template="jsc" >}}
### Pas copier-coller `StackOverFlow`

98% snippets sur la sécurité/crypto
sont *insecures*

Fisher et al., 2017; Nadi et al., 2016; Das et al, 2014
[Prevent cryptographic pitfalls by design](https://archive.fosdem.org/2019/schedule/event/crypto_pitfalls/)

---
{{< slide template="jsc" >}}
#### Attention avec Docker
![Docker vulnerabilities](img/snyk_rapport.png)
[The state of open source security – 2019](https://snyk.io/opensourcesecurity-2019/)

---
{{< slide template="jsc" >}}
#### Attention avec vos dépendences
![Dependencies vulnerabilities](img/snyk_dep.png)
[The state of open source security – 2019](https://snyk.io/opensourcesecurity-2019/)

---
{{< slide template="jsc" >}}
###### 
![ImageMagick flaw for months costs Facebook $40k](img/facebook_dep.png)
[](https://www.pcworld.com/article/3158861/failure-to-patch-known-imagemagick-flaw-for-months-costs-facebook-40k.html)

{{% /section %}}

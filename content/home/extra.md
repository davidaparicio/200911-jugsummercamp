+++
weight = 7
+++
{{< slide template="jsc" >}}
{{% section %}}
#### Exfiltration DNS 

[@Rob Sobers (Twitter)](https://twitter.com/rsobers/status/1293539543115862016)

![DNS exfiltration](img/dns_exfiltration.jpg)

---
{{< slide template="jsc" >}}
#### Pas de MEP / Failure Fridays

![DevOps](img/misfortune.png)

[PagerDuty, 121, 200 tickets opened, 3 full AZ failures](https://www.pagerduty.com/blog/failure-fridays-four-years/)

---
{{< slide template="jsc" >}}
#### Ex: Maturité des équipes
![Maturity Matrix](img/maturity.png)

{{% /section %}}
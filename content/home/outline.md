+++
weight = 2
+++
{{< slide template="jsc" >}}
{{% section %}}
## Pitch 

> Salut à toi jeune développeur(euse) ! Alors si aujourd'hui je me permets de te contacter, c'est pour une raison très simple: savais-tu que 95% des failles de sécurité sont dues à une erreur humaine ? 
<!-- En dehors du titre, le générique masculin est utilisé sans aucune discrimination et uniquement dans le but d'alléger le texte. -->

[IBM's Cyber Security report](https://www.techrepublic.com/article/ibm-says-most-security-breaches-are-eue-to-human-error/)
<!--[Cyberint](https://www.cybintsolutions.com/cyber-security-facts-stats/)-->
---
> Alors est-ce que tu veux <!-- ton entreprise !--> en faire parti ? Il faut que tu te poses les bonnes questions.
{{% /section %}}

<!--
---
{{< slide template="jsc" >}}
## Qui êtes-vous ?
{{% fragment %}}* Dev{{% /fragment %}}
{{% fragment %}}* Ops{{% /fragment %}}
{{% fragment %}}* DevOps{{% /fragment %}}
{{% fragment %}}* DevSecOps{{% /fragment %}}
-->

---
{{< slide template="jsc" >}}
* Définition
* Best Practices
* Outils
* Scénarios
* Q/A

+++
weight = 4
+++
{{< slide template="jsc" >}}

## Bonnes pratiques

{{% fragment %}}* Principe de moindre privilège !root {{% /fragment %}}
{{% fragment %}}* Diminuer surface d'attaque (scratch, distroless){{% /fragment %}}
{{% fragment %}}* Pas de secrets dans les Docker images {{% /fragment %}}
{{% fragment %}}* Pas de données sensibles dans les GUI{{% /fragment %}}
{{% fragment %}}* Mettre à jour infra/docker images (CI/CD|[GitOps](https://www.infoq.com/news/2020/02/wksctl-kubernetes-gitops/)) {{% /fragment %}}
{{% fragment %}}* Ne pas afficher de stacktrace (pas debug) {{% /fragment %}}
{{% fragment %}}* Ni de version/nom de framework {{% /fragment %}}
{{% fragment %}}* Vérifier les entrées/sorties (injection/XSS) {{% /fragment %}}
{{% fragment %}}* PaaS (BUILD/RUN) 🇪🇺 OVHCloud/CleverCloud {{% /fragment %}}

---
{{< slide template="jsc" >}}
![OWASP](img/keys.png)

---
{{< slide template="jsc" >}}
## Prendre du recul /code
![C](img/code_c.png)

---
{{< slide template="jsc" >}}

> «David, c'est quand que tu vas mettre des paillettes dans ma vie ?»

---
{{< slide template="jsc" >}}
## Outils

{{% fragment %}}[linter](https://en.wikipedia.org/wiki/Lint_(software))/[npm-audit](https://docs.npmjs.com/auditing-package-dependencies-for-security-vulnerabilities) (Code),
[SonarQube](https://www.sonarqube.org/) (QA),
[Gitlab SAST](https://docs.gitlab.com/ee/user/application_security/sast/) /[Argo](https://argoproj.github.io/)/[GitHub](https://github.com/features/security) (CI),
[Saucs](https://www.saucs.com/)/[Clair](https://coreos.com/clair/docs/latest/)/[Anchore](https://anchore.com/)/[Dagda](https://github.com/eliasgranderubio/dagda) (CVE),{{% /fragment %}}
{{% fragment %}}[OpenSCAP](https://www.open-scap.org/) (Audit),
[Cilium](https://cilium.io/) (Network),
[gVisor](https://github.com/google/gvisor)/[Kata](https://katacontainers.io/) (Sandbox),
[Istio](https://istio.io)/[maesh](https://containo.us/maesh/) (SSL),
[Notary](https://docs.docker.com/notary/getting_started/) (Sign.),{{% /fragment %}}
{{% fragment %}}[Falco](https://sysdig.com/opensource/falco/) (K8s Monitor),
[42Crunch](https://42crunch.com/api-security/) (API Scan),
[Burp Suite](https://portswigger.net/burp) /[SuperTruder](https://github.com/ElSicarius/SuperTruder)/[ffuf](https://github.com/ffuf/ffuf) (Vuln.Web),
[OWASP ZAP](https://www.zaproxy.org/) (Proxy),{{% /fragment %}}
{{% fragment %}}[GitGuardian](https://www.gitguardian.com/) (Secret),
[Parrot](https://www.parrotsec.org/),
[Kali](https://www.kali.org/),
[RedHat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/index) (OS),
[YesWeHack](https://www.yeswehack.com/),
[Yogosha](https://yogosha.com/) (Bounty){{% /fragment %}}

---
{{< slide template="jsc" >}}
{{% section %}}
## Docker CLI
![Docker Scan](img/docker_scan.png)
[Vulnerability scanning for Docker local images | Docker Documentation](https://docs.docker.com/engine/scan/)

---
{{< slide template="jsc" >}}
## Sonar
![Sonar](img/sonar.png)

---
{{< slide template="jsc" >}}
## CI/CD
![Gitlab](img/pipeline.jpg)

<!--
---
## Gitlab

Security framework
GDPR - General Data Protection Regulation
HIPAA - Health Insurance Portability and Accountability Act
PCI-DSS - Payment Card Industry-Data Security Standard
SOC 2 - Service Organization Control 2
SOX - Sarbanes-Oxley

Static Application Security Testing (SAST)
Analyze your source code for known vulnerabilities.
Dynamic Application Security Testing (DAST)
Analyze a review version of your web application.

Dependency/Container/Secret Scanning
Analyze your dependencies for known vulnerabilities.
Check your Docker images for known vulnerabilities.
Analyze your source code and git history for secrets.

Coverage Fuzzing/License Compliance
Find bugs in your code with coverage-guided fuzzing
Search your project dependencies for their licenses and apply policies.
-->

---
{{< slide template="jsc" >}}
## Falco
![Falco](img/falco.png)
[Kris Nova, Fixing the Kubernetes clusterfuck @FOSDEM](https://fosdem.org/2020/schedule/event/kubernetes/)

{{% /section %}}

# Timing rehearsal

## First time, first blood

* Total : 9m53s
* Init  : 26s
* Defin : 2m23s
* Best  : 1m53s
* Tools : 3m32s
* S/Con : 1m41s

## Second time, with horacio

* Total : 10m38s
* Init  : 40s
* Defin : 2m00s
* Best  : 1m50s
* Tools : 4m52s
* S/Con : 1m14s

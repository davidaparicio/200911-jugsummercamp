# La sécurité dès la conception du projet

## 20-09-11 : Jug Summer Camp 2020

PENSER UNE DÉMARCHE PRÉVENTIVE DÈS LA PHASE DE CONCEPTION D’UN PROJET
--

La protection des données personnelles ainsi que la sécurité "by design" deviennent des sujets centraux, depuis l'entrée en vigueur du RGPD.

Je présenterai les techniques utilisées et j'aborderai aussi la prise de conscience du management sur ces aspects importants.

En effet, même en étant dans le réseau interne d’un datacenter, nous ne sommes pas à l’abri d’une intrusion. Le risque 0 n'existe pas.

##### ~ Security By Design 101